<h1 class="code-line" data-line-start=0 data-line-end=1 ><a id="Turkcell_Assignment_0"></a>Turkcell Assignment</h1>
<p class="has-line-data" data-line-start="2" data-line-end="3">In this project, the request detailed below has been developed with the VIPER design pattern.</p>
<h2 class="code-line" data-line-start=4 data-line-end=5 ><a id="Assignment_Details_4"></a>Assignment Details</h2>
<p class="has-line-data" data-line-start="6" data-line-end="8">The goal of this assignment is to assess your mobile application development knowledge and general programming style.<br>
Build a simple application that displays the list of products and detail page of selected product. Things you have to take into consideration:</p>
<p class="has-line-data" data-line-start="9" data-line-end="17">• Implementing using MVVM or VIPER design pattern.;<br>
• Performance;<br>
• Image cache;<br>
• Exception handling.<br>
• Offline/caching strategies. (Core Data Usage)<br>
• Do not use any third party library.<br>
• Detail screen with a larger version of the image, the full title and the description.<br>
• Unit tests</p>
<p class="has-line-data" data-line-start="18" data-line-end="20">You can obtain the list of products from the following REST endpoint with HTTP GET method:<br>
<a href="https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/list">https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/list</a></p>
<p class="has-line-data" data-line-start="21" data-line-end="23">A product can be obtained from the following endpoint with HTTP GET method:<br>
<a href="https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/%7Bproduct_id%7D/detail">https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/{product_id}/detail</a></p>
