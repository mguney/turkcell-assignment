//
//  ServiceManager.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 21.09.2021.
//

import Foundation


public protocol ServiceProtocol {
    func getProductList(completion: @escaping (Result<[ProductModel]>) -> Void)
    func getProductDetail(id: String, completion: @escaping (Result<ProductDetailModel>) -> Void)
    func getImage(productId: String, url: String, completion: @escaping (Result<NSData?>) -> Void)
}

///This class is used to retrieve data.
public class ServiceManager: ServiceProtocol {
    
    public static let instance: ServiceManager = ServiceManager()
    private let apiManager: APIService
    
    init() {
        self.apiManager = APIService()
    }
    
    /// Gets the product list
    ///
    /// - Parameters:
    ///   - completion: The closure to be executed once fetching the list.
    public func getProductList(completion: @escaping (Result<[ProductModel]>) -> Void) {
        if let productList = CoreDataService().getProductList(), !productList.isEmpty {
            completion(.success(productList))
            return
        }
        
        apiManager.getProductList(completion: {[weak self] result in
            guard self != nil else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let productList):
                CoreDataService().saveProductList(productList: productList)
                completion(.success(productList))
            }
        })
        
    }
    
    /// Gets the product detail
    ///
    /// - Parameters:
    ///   - id: Identity of the product to be detailed.
    ///   - completion: The closure to be executed once fetching detail.
    public func getProductDetail(id: String, completion: @escaping (Result<ProductDetailModel>) -> Void) {
        if let productDetail = CoreDataService().getProductDetail(id: id) {
            completion(.success(productDetail))
            return
        }
         
        apiManager.getProductDetail(id: id, completion: {[weak self] result in
            guard self != nil else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let productDetail):
                CoreDataService().saveProductDetail(productDetail: productDetail)
                completion(.success(productDetail))
            }
        })
    }
    
    /// Gets the product image
    ///
    /// - Parameters:
    ///   - productId: Identity of the product to be detailed.
    ///   - url: URL of the product image.
    ///   - completion: The closure to be executed once fetching image
    public func getImage(productId: String, url: String, completion: @escaping (Result<NSData?>) -> Void) {
        if let imageData = CoreDataService().getProductImage(id: productId) {
            completion(.success(imageData))
            return
        }
        
        apiManager.downloadImage(url: url, completion: {[weak self] result in
            guard self != nil else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                CoreDataService().saveProductImage(id: productId, data: data as NSData)
                completion(.success(data as NSData))
            }
        })
    }
    
}
