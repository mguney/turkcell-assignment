//
//  CoreDataService.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 21.09.2021.
//

import UIKit
import CoreData

///This class is used to connect to the CoreData and retrieve data.
public class CoreDataService {
    
    /// Gets the product list
    public func getProductList() -> [ProductModel]? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Product")
        
        do {
            let fetchResults = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest) as! [NSManagedObject]
            return fetchResults.map(ProductModel.init)
            
        } catch {
            return nil
        }
    }
    
    /// Save the product list
    ///
    /// - Parameters:
    ///   - productList: List to be saved.
    public func saveProductList(productList: [ProductModel]) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Product", in: appDelegate.persistentContainer.viewContext)!
        
        for product in productList {
            let item = NSManagedObject(entity: entityDescription, insertInto: appDelegate.persistentContainer.viewContext)
            item.setValue(product.productId, forKey: "productId")
            item.setValue(product.name, forKey: "name")
            item.setValue(product.price, forKey: "price")
            item.setValue(product.imageURL, forKey: "imageURL")
        }
        
        do{
            try appDelegate.persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
    
    /// Gets the product detail
    ///
    /// - Parameters:
    ///   - id: Identity of the product to be detailed.
    public func getProductDetail(id: String) -> ProductDetailModel? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        
        let predicate = NSPredicate(format: "productId == %@", id)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Detail")
        fetchRequest.predicate = predicate
        
        do{
            let fetchResults = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
            guard let product = fetchResults.first as? NSManagedObject else {
                return nil
            }
            return ProductDetailModel(product: product)
        } catch {
            return nil
        }
    }
    
    /// Save the product detail
    ///
    /// - Parameters:
    ///   - productDetail: Product to be saved.
    public func saveProductDetail(productDetail: ProductDetailModel) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Detail", in: appDelegate.persistentContainer.viewContext)!
        let item = NSManagedObject(entity: entityDescription, insertInto: appDelegate.persistentContainer.viewContext)
        item.setValue(productDetail.productId, forKey: "productId")
        item.setValue(productDetail.name, forKey: "name")
        item.setValue(productDetail.price, forKey: "price")
        item.setValue(productDetail.imageURL, forKey: "imageURL")
        item.setValue(productDetail.description, forKey: "desc")
        
        do {
            try appDelegate.persistentContainer.viewContext.save()
        } catch let error{
            print(error)
        }
    }
    
    /// Gets the product
    ///
    /// - Parameters:
    ///   - productId: Identity of the product.
    private func getProduct(productId: String) -> NSManagedObject? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        
        let predicate = NSPredicate(format: "productId == %@", productId)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Product")
        fetchRequest.predicate = predicate
        
        do{
            let fetchResults = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
            guard let product = fetchResults.first as? NSManagedObject else {
                return nil
            }
            return product
        } catch {
            return nil
        }
    }
    
    /// Gets the products image
    ///
    /// - Parameters:
    ///   - id: Identity of the product.
    public func getProductImage(id: String) -> NSData? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        
        let predicate = NSPredicate(format: "productId == %@", id)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Product")
        fetchRequest.predicate = predicate
        
        do{
            let fetchResults = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
            guard let product = fetchResults.first as? NSManagedObject, let imageData = product.value(forKey: "imageData") as? NSData else {
                return nil
            }
            return imageData
        } catch {
            return nil
        }
    }
    
    /// Save the products image
    ///
    /// - Parameters:
    ///   - id: Identity of the product.
    ///   - data: Data of the products image.   
    public func saveProductImage(id: String, data: NSData) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let predicate = NSPredicate(format: "productId == %@", id)
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Product")
        fetchRequest.predicate = predicate
        
        do {
            let fetchResults = try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
            guard let product = fetchResults.first as? NSManagedObject else {
                return
            }
            product.setValue(data, forKey: "imageData")
            try appDelegate.persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
}
