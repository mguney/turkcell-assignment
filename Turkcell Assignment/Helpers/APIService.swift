//
//  APIService.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

///This class is used to connect to the service and retrieve data.
public class APIService {
    
    private static let PRODUCT_LIST_URL = "https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/list"
    private static let PRODUCT_DETAIL_URL = "https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/%@/detail"
    
    /// Gets the product list
    ///
    /// - Parameters:
    ///   - completion: The closure to be executed once fetching the list.
    public func getProductList(completion: @escaping (Result<[ProductModel]>) -> Void) {
        
        self.fetch(url: APIService.PRODUCT_LIST_URL, completion: {[weak self] result in
            guard self != nil else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                let decoder = JSONDecoder()
                do {
                    let response = try decoder.decode(ProductResponseModel.self, from: data)
                    completion(.success(response.products))
                } catch let error {
                    completion(.failure(error))
                }
            }
        })
    }
    
    /// Gets the product detail
    ///
    /// - Parameters:
    ///   - id: Identity of the product to be detailed.
    ///   - completion: The closure to be executed once fetching detail.
    public func getProductDetail(id: String, completion: @escaping (Result<ProductDetailModel>) -> Void) {
        self.fetch(url: String(format: APIService.PRODUCT_DETAIL_URL, id), completion: {[weak self] result in
            guard self != nil else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                let decoder = JSONDecoder()
                
                do {
                    let productDetail = try decoder.decode(ProductDetailModel.self, from: data)
                    completion(.success(productDetail))
                } catch let error {
                    completion(.failure(error))
                }
            }
        })
    }
    
    /// Downloads the product image
    ///
    /// - Parameters:
    ///   - id: Identity of the product
    ///   - completion: The closure to be executed once fetching image
    public func downloadImage(url: String, completion: @escaping (Result<Data>) -> Void) {
        fetch(url: url, completion: {[weak self] result in
            guard self != nil else { return }
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                completion(.success(data))
            }
        })
    }
    
    /// Downloads the product image
    ///
    /// - Parameters:
    ///   - url: URL of the data.
    ///   - completion: The closure to be executed once fetching data.
    private func fetch(url: String, completion: @escaping (Result<Data>) -> Void) {
        guard let url = URL(string: url) else { return }
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: url) { [weak self] data, response, error in
            guard self != nil else { return }
            
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(.failure(error ?? NSError()))
                }
                return
            }
            
            if let error = error {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
            
            DispatchQueue.main.async {
                completion(.success(data))
            }
        }
        
        task.resume()
    }
}
