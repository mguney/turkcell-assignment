//
//  ProductCell.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import UIKit
import Foundation

class ProductCell: UICollectionViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func prepare(product: ProductViewModel) {
        titleLabel.text = product.name
        priceLabel.text = "\(product.price)"
        if let imageData = product.imageData, let data = imageData as Data?, let image = UIImage(data: data) {
            productImageView.image = image
        }
    }
}
