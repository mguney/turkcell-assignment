//
//  ProductListProtocols.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

//MARK: Router

protocol ProductListRouterProtocol: AnyObject {
    func showDetail(productId: String)
}

//MARK: View

protocol ProductListViewProtocol: AnyObject {
    func showLoading()
    func hideLoading()
    func showProductList(productList: [ProductViewModel])
    func showImage(index: Int, data: Data?)
    func showError(error: Error)
}

//MARK: Presenter

protocol ProductListPresenterProtocol: AnyObject {
    func load()
    func selectProduct(index: Int)
    func getImage(index: Int)
}

//MARK: Interactor

protocol ProductListInteractorOutPutProtocol: AnyObject {
    func showLoading()
    func hideLoading()
    func showProductList(productList: [ProductModel])
    func showProductDetail(productId: String)
    func showImage(index: Int, data: Data?)
    func showError(error: Error)
}

protocol ProductListInteractorProtocol: AnyObject {
    var delegate: ProductListInteractorOutPutProtocol? { get set }
    func load()
    func selectProduct(index: Int)
    func getImage(index: Int)
}




