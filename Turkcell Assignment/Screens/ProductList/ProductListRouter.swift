//
//  ProductListRouter.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

final class ProductListRouter: ProductListRouterProtocol {
    
    unowned let view: ProductListViewController
    
    init(view: ProductListViewController) {
        self.view = view
    }
    
    static func build() -> ProductListViewController {
        let view = ProductListViewController()
        let router = ProductListRouter(view: view)
        let interactor = ProductListInteractor(service: ServiceManager.instance)
        let presenter = ProductListPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        
        return view
    }
    
    func showDetail(productId: String) {
        let detailView = ProductDetailRouter.build(productId: productId)
        view.show(detailView, sender: nil)
    }
}
