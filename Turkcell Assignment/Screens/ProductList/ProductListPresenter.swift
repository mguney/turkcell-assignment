//
//  ProductListPresenter.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

final class ProductListPresenter: ProductListPresenterProtocol {
    
    private unowned let view: ProductListViewProtocol
    private let interactor: ProductListInteractorProtocol
    private let router: ProductListRouterProtocol
    
    init(view: ProductListViewProtocol, interactor: ProductListInteractorProtocol, router: ProductListRouterProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor
        self.interactor.delegate = self
    }
    
    func load() {
        interactor.load()
    }
    
    func selectProduct(index: Int) {
        interactor.selectProduct(index: index)
    }
    
    func getImage(index: Int) {
        interactor.getImage(index: index)
    }
}

extension ProductListPresenter: ProductListInteractorOutPutProtocol {
    func hideLoading() {
        view.hideLoading()
    }
    
    func showLoading() {
        view.showLoading()
    }
    
    func showProductList(productList: [ProductModel]) {
        view.showProductList(productList: productList.map(ProductViewModel.init))
    }
    
    func showProductDetail(productId: String) {
        router.showDetail(productId: productId)
    }
    
    func showImage(index: Int, data: Data?) {
        view.showImage(index: index, data: data)
    }
    
    func showError(error: Error) {
        view.showError(error: error)
    }
}
