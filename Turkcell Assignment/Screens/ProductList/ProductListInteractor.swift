//
//  ProductListInteractor.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

final class ProductListInteractor: ProductListInteractorProtocol {
    weak var delegate: ProductListInteractorOutPutProtocol?
    private let service: ServiceProtocol
    private var productList: [ProductModel] = []
    
    init(service: ServiceProtocol) {
        self.service = service
    }
    
    func load() {
        delegate?.showLoading()
        service.getProductList(completion: {[weak self] result in
            guard let self = self else { return }
            self.delegate?.hideLoading()
            
            switch result {
            case .failure(let error):
                self.delegate?.showError(error: error)
                return
            case .success(let products):
                self.productList = products
                self.delegate?.showProductList(productList: products)
            }
        })
    }
    
    func selectProduct(index: Int) {
        let product = productList[index]
        delegate?.showProductDetail(productId: product.productId)
    }
    
    func getImage(index: Int) {
        let product = productList[index]
        
        ImageCache.publicCache.load(item: product, completion: {[weak self] fetchedItem, data in
            guard let self = self else { return }
            self.delegate?.showImage(index: index, data: data as Data?)
        })
    }
}
