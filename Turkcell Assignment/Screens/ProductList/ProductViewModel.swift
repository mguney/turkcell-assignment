//
//  ProductViewModel.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

final class ProductViewModel: NSObject {
    let productId: String
    let name: String
    let price: Decimal
    let imageURL: String
    var imageData: NSData?
    
    init(productId: String, name: String, price: Decimal, imageURL: String, imageData: NSData?) {
        self.productId = productId
        self.name = name
        self.price = price
        self.imageURL = imageURL
        self.imageData = imageData
        super.init()
    }
}

extension ProductViewModel {
    convenience init(product: ProductModel) {
        self.init(productId: product.productId, name: product.name, price: product.price, imageURL: product.imageURL, imageData: nil)
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? ProductViewModel else { return false }
        return self.productId == object.productId
    }
}

enum Section {
    case base
}
