//
//  ProductListViewController.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import UIKit

class ProductListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter: ProductListPresenter!
    var productList: [ProductViewModel] = []
    let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Product List"
        
        addIndicator()
        prepareCollectionView()
        presenter.load()
    }
    
    func addIndicator() {
        indicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        indicator.center = view.center
        self.view.addSubview(indicator)
        self.view.bringSubviewToFront(indicator)
    }
    
    func prepareCollectionView() {
        collectionView.collectionViewLayout = createLayout()
        collectionView.register(UINib(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "ProductCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
        let product = productList[indexPath.row]
        cell.prepare(product: product)
        
        if product.imageData == nil {
            presenter.getImage(index: indexPath.row)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.selectProduct(index: indexPath.row)
    }
    
    private func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.5),
                                              heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .fractionalWidth(0.5))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                       subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
}

extension ProductListViewController: ProductListViewProtocol {
    func showLoading() {
        indicator.startAnimating()
    }
    
    func hideLoading() {
        indicator.stopAnimating()
    }
    
    func showProductList(productList: [ProductViewModel]) {
        self.productList = productList
        collectionView.reloadData()
        //getImages()
    }
    
    //Since UI performance problems are experienced when taking pictures in the 'cellForItemAt' method, the related pictures are taken with this method.
    func getImages() {
        for index in 0...(productList.count-1) {
            presenter.getImage(index: index)
        }
    }
    
    func showImage(index: Int, data: Data?) {
        let product = productList[index]
        if let data = data as NSData?, data != product.imageData, let image = UIImage(data: data as Data) {
            product.imageData = image.getThumbnail()?.pngData() as NSData?
            self.collectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
        }
    }
    
    func showError(error: Error) {
        let alert = UIAlertController(title: "Error", message: "An unexpected error has occurred. Try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
