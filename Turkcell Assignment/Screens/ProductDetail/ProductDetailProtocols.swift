//
//  ProductDetailProtocols.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

//MARK: Router

protocol ProductDetailRouterProtocol: AnyObject {
    
}

//MARK: View

protocol ProductDetailViewProtocol: AnyObject {
    func showLoading()
    func hideLoading()
    func showDetail(productDetail: ProductDetailViewModel)
    func showImage(data: Data?)
    func showError(error: Error)
}

//MARK: Presenter

protocol ProductDetailPresenterProtocol: AnyObject {
    func load()
    func getImage()
}

//MARK: Interactor

protocol ProductDetailInteractorProtocol: AnyObject {
    var delegate: ProductDetailInteractorOutPutProtocol? { get set }
    func load(productId: String)
    func getImage()
}

protocol ProductDetailInteractorOutPutProtocol: AnyObject {
    func showLoading()
    func hideLoading()
    func showProductDetail(product: ProductDetailModel)
    func showImage(data: Data?)
    func showError(error: Error)
}
