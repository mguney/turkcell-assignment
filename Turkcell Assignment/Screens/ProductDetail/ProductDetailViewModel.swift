//
//  ProductDetailViewModel.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

final class ProductDetailViewModel: NSObject {
    let productId: String
    let name: String
    let price: Decimal
    let imageURL: String
    let productDescription: String
    var imageData: NSData?
    
    init(productId: String, name: String, price: Decimal, imageURL: String, description: String, imageData: NSData?) {
        self.productId = productId
        self.name = name
        self.price = price
        self.imageURL = imageURL
        self.productDescription = description
        self.imageData = imageData
        super.init()
    }
}

extension ProductDetailViewModel {
    convenience init(product: ProductDetailModel) {
        self.init(productId: product.productId, name: product.name, price: product.price, imageURL: product.imageURL, description: product.description ?? "", imageData: nil)
    }
}
