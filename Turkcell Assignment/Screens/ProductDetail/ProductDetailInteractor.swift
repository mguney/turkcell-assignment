//
//  ProductDetailInteractorProtocol.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

final class ProductDetailInteractor: ProductDetailInteractorProtocol {
    weak var delegate: ProductDetailInteractorOutPutProtocol?
    private let service: ServiceProtocol
    private var productDetail: ProductDetailModel?
    
    init(service: ServiceProtocol) {
        self.service = service
    }
    
    func load(productId: String) {
        delegate?.showLoading()
        service.getProductDetail(id: productId, completion: {[weak self] result in
            guard let self = self else { return }
            self.delegate?.hideLoading()
            
            switch result {
            case .failure(let error):
                self.delegate?.showError(error: error)
            case .success(let productDetail):
                self.productDetail = productDetail
                self.delegate?.showProductDetail(product: productDetail)
            }
            
        })
    }
    
    func getImage() {
        
        guard let detail = productDetail else {
            delegate?.showImage(data: nil)
            return
        }
        
        ImageCache.publicCache.load(id: detail.productId, imageURL: detail.imageURL, completion: {[weak self] data in
            guard let self = self else { return }
            self.delegate?.showImage(data: data as Data?)
        })
    }
    
}
