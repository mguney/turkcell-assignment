//
//  ProductDetailPresenter.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

final class ProductDetailPresenter: ProductDetailPresenterProtocol {
    
    private unowned let view: ProductDetailViewProtocol
    private let interactor: ProductDetailInteractorProtocol
    private let router: ProductDetailRouterProtocol
    private let productId: String
    
    init(view: ProductDetailViewProtocol, interactor: ProductDetailInteractorProtocol, router: ProductDetailRouterProtocol, productId: String) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.productId = productId
        self.interactor.delegate = self
    }
    
    func load() {
        interactor.load(productId: productId)
    }
    
    func getImage() {
        interactor.getImage()
    }
}

extension ProductDetailPresenter: ProductDetailInteractorOutPutProtocol {
    func showLoading() {
        view.showLoading()
    }
    
    func hideLoading() {
        view.hideLoading()
    }
    
    func showProductDetail(product: ProductDetailModel) {
        view.showDetail(productDetail: ProductDetailViewModel.init(product: product))
    }
    
    func showImage(data: Data?) {
        view.showImage(data: data)
    }
    
    func showError(error: Error) {
        view.showError(error: error)
    }
    
}
