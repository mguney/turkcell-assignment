//
//  ProductDetailViewController.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import UIKit

class ProductDetailViewController: UIViewController {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var presenter: ProductDetailPresenter!
    let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
    var productDetail: ProductDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Product Detail"
        addIndicator()
        presenter.load()
    }
    
    func addIndicator() {
        indicator.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        indicator.center = view.center
        self.view.addSubview(indicator)
        self.view.bringSubviewToFront(indicator)
    }
}

extension ProductDetailViewController: ProductDetailViewProtocol {
    func showLoading() {
        indicator.startAnimating()
    }
    
    func hideLoading() {
        indicator.stopAnimating()
    }
    
    func showDetail(productDetail: ProductDetailViewModel) {
        self.productDetail = productDetail
        productImageView.image = UIImage()
        descriptionLabel.sizeToFit()
        descriptionLabel.text = productDetail.productDescription
        navigationItem.title = productDetail.name
        guard productDetail.imageData != nil else {
            presenter.getImage()
            return
        }
    }
    
    func showImage(data: Data?) {
        if let imageData = data, let _ = imageData as Data?, let image = UIImage(data: imageData) {
            self.productImageView.image = image
            self.productDetail?.imageData = data as NSData?
        }
    }
    
    func showError(error: Error) {
        let alert = UIAlertController(title: "Error", message: "An unexpected error has occurred. Try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
