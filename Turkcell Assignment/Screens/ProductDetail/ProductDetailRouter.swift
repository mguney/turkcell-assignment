//
//  ProductDetailRouter.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

final class ProductDetailRouter: ProductDetailRouterProtocol {
    
    static func build(productId: String) -> ProductDetailViewController {
        let view = ProductDetailViewController()
        let router = ProductDetailRouter()
        let interactor = ProductDetailInteractor(service: ServiceManager.instance)
        let presenter = ProductDetailPresenter(view: view, interactor: interactor, router: router, productId: productId)
        
        view.presenter = presenter
        
        return view
    }
}
