//
//  ProductDetail.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation
import CoreData

public struct ProductDetailModel: Decodable {
    
    enum CodingKeys : String, CodingKey {
        case productId = "product_id"
        case name
        case price
        case imageURL = "image"
        case description = "description"
    }
    
    let productId: String
    let name: String
    let price: Decimal
    let imageURL: String
    let description: String?
}

extension ProductDetailModel {
    init(product: NSManagedObject) {
        self.init(productId: product.value(forKey: "productId") as! String,
                  name: product.value(forKey: "name") as! String,
                  price: product.value(forKey: "price") as! Decimal,
                  imageURL: product.value(forKey: "imageURL") as! String,
                  description: product.value(forKey: "desc") as? String)
    }
}
