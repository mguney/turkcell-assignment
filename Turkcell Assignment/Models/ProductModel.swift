//
//  Product.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation
import CoreData

public struct ProductModel: Decodable, Equatable {
    
    enum CodingKeys : String, CodingKey {
        case productId = "product_id"
        case name
        case price
        case imageURL = "image"
    }
    
    let productId: String
    let name: String
    let price: Decimal
    let imageURL: String
}

extension ProductModel {
    init(product: NSManagedObject) {
        self.init(productId: product.value(forKey: "productId") as! String,
                  name: product.value(forKey: "name") as! String,
                  price: product.value(forKey: "price") as! Decimal,
                  imageURL: product.value(forKey: "imageURL") as! String)
    }
}
