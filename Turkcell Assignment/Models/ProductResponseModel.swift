//
//  ProductResponse.swift
//  Turkcell Assignment
//
//  Created by Mahmut Güney on 20.09.2021.
//

import Foundation

struct ProductResponseModel: Decodable {
    let products: [ProductModel]
}
