//
//  MockStoreManager.swift
//  Turkcell AssignmentTests
//
//  Created by Mahmut Güney on 21.09.2021.
//

import Foundation
@testable import Turkcell_Assignment

final class MockStoreManager: ServiceProtocol {
    
    var products: [ProductModel] = []
    var productDetail: ProductDetailModel = ProductDetailModel(productId: "1", name: "Steak", price: 543, imageURL: "test", description: "test")
    var imageData: NSData = NSData()
    
    func getProductList(completion: @escaping (Result<[ProductModel]>) -> Void) {
        completion(.success(products))
    }
    
    func getProductDetail(id: String, completion: @escaping (Result<ProductDetailModel>) -> Void) {
        completion(.success(productDetail))
    }
    
    func getImage(productId: String, url: String, completion: @escaping (Result<NSData?>) -> Void) {
        completion(.success(imageData))
    }
}
