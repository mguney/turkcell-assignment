//
//  Turkcell_AssignmentTests.swift
//  Turkcell AssignmentTests
//
//  Created by Mahmut Güney on 20.09.2021.
//

import XCTest
@testable import Turkcell_Assignment

class Turkcell_AssignmentTests: XCTestCase {
    
    private var service: MockStoreManager!
    private var productListPresenter: ProductListPresenter!
    private var productListInteractor: ProductListInteractor!
    private var productListView: MockProductListView!
    private var productListRouter: MockProductListRouter!
    
    private var productDetailPresenter: ProductDetailPresenter!
    private var productDetailInteractor: ProductDetailInteractor!
    private var productDetailView: MockProductDetailView!
    private var productDetailRouter: ProductDetailRouter!
    
    override func setUp() {
        service = MockStoreManager()
        
        productListInteractor = ProductListInteractor(service: service)
        productListView = MockProductListView()
        productListRouter = MockProductListRouter()
        productListPresenter = ProductListPresenter(view: productListView, interactor: productListInteractor, router: productListRouter)
        productListView.presenter = productListPresenter
        
        productDetailInteractor = ProductDetailInteractor(service: service)
        productDetailView = MockProductDetailView()
        productDetailRouter = ProductDetailRouter()
        productDetailPresenter = ProductDetailPresenter(view: productDetailView, interactor: productDetailInteractor, router: productDetailRouter, productId: "1")
        productDetailView.presenter = productDetailPresenter
    }
    
    func testProductListLoad() throws {
        // Given:
        service.products = [ProductModel(productId: "1", name: "Steak", price: 543, imageURL: "test"), ProductModel(productId: "2", name: "Pork", price: 343, imageURL: "test")]
        
        // When:
        productListView.viewDidLoad()
        
        // Then:
        XCTAssertEqual(productListView.isCallShowLoading, true)
        XCTAssertEqual(productListView.isCallHideLoading, true)
        XCTAssertEqual(productListView.productList.count, service.products.count)
    }
    
    func testSelectProduct() throws {
        // Given:
        service.products = [ProductModel(productId: "1", name: "Steak", price: 543, imageURL: "test"), ProductModel(productId: "2", name: "Pork", price: 343, imageURL: "test")]
        productListView.viewDidLoad()
        
        // When:
        productListView.selectProduct(index: 0)
        
        // Then:
        XCTAssertEqual(service.products[0].productId, productListRouter.productId)
    }
    
    func testProductDetailLoad() throws {
        // Given:
        service.products = [ProductModel(productId: "1", name: "Steak", price: 543, imageURL: "test"), ProductModel(productId: "2", name: "Pork", price: 343, imageURL: "test")]
        
        // When:
        productDetailView.viewDidLoad()
        
        // Then:
        XCTAssertEqual(productDetailView.isCallShowLoading, true)
        XCTAssertEqual(productDetailView.isCallHideLoading, true)
        XCTAssertEqual(productDetailView.productDetail?.productId, service.products[0].productId)
    }
}

private final class MockProductListView: ProductListViewProtocol {
    
    var presenter: ProductListPresenter!
    
    func viewDidLoad() {
        presenter.load()
    }
    
    var isCallShowLoading: Bool = false
    var isCallHideLoading: Bool = false
    var productList: [ProductViewModel] = []
    
    func showLoading() {
        isCallShowLoading = true
    }
    
    func hideLoading() {
        isCallHideLoading = true
    }
    
    func showProductList(productList: [ProductViewModel]) {
        self.productList = productList
    }
    
    func selectProduct(index: Int) {
        presenter.selectProduct(index: index)
    }
    
    func showImage(index: Int, data: Data?) {
    }
    
    func showError(error: Error) {
        
    }
}

private final class MockProductListRouter: ProductListRouterProtocol {
    
    var productId: String = ""
    
    func showDetail(productId: String) {
        self.productId = productId
    }
}


private final class MockProductDetailView: ProductDetailViewProtocol {
    
    var presenter: ProductDetailPresenter!
    var isCallShowLoading: Bool = false
    var isCallHideLoading: Bool = false
    var productDetail: ProductDetailViewModel? = nil
    
    func viewDidLoad() {
        presenter.load()
    }
    
    func showLoading() {
        isCallShowLoading = true
    }
    
    func hideLoading() {
        isCallHideLoading = true
    }
    
    func showDetail(productDetail: ProductDetailViewModel) {
        self.productDetail = productDetail
    }
    
    func showImage(data: Data?) {
        
    }
    
    func showError(error: Error) {
        
    }
}
